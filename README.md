### Using the package
Require package
```php
composer require --dev strangefate/foundationprep
```

Before running the foundation install command, you should initilize your ui and js components.
```php
php artisan ui vue --auth
```

Run foundation installer in call npm installer for Zurb Foundation packages and configuration items
```php
php artisan foundation:install
```

### The installer will make the following changes to your current deployment.

First, the installer will execute an npm install for zurb foundation with the Dependencies and fontawesome.
```javascript
npm install --save-dev @fortawesome/fontawesome-free foundation-sites what-input motion-ui
```

Publish templates using the force override option
```php
//This command is run automatically as part of the foundation:install command
php artisan vendor:publish --provider=StrangeFate\FoundationPrep\FoundationPrepServiceProvider --force
```

The Foundation SASS file will be copied over and update in settings.
```php
copy '/node_modules/foundation-sites/scss/settings/_settings.scss' resource_path('sass/_settings.scss')
```

Update app.js file with Foundation and what-input to **resources\js\bootstrap.js**, and remove bootstrap and popper calls.
```javascript
try {
    window.$ = window.jQuery = require('jquery');
    window.Vue = require('vue');

    //require these packages
    require('foundation-sites');
    require('what-input');

    //remove these packages
    //require('bootstrap');
    //window.Popper = require('popper.js').default; 
} catch (e) {}
```

Call foundation at the end of you **resources\js\app.js** file, or in the Vue mounted function if you plan to use Vue components.
```javascript
const app = new Vue({
    el: '#app',
    mounted() {
    	$(document).foundation(); //With Vue components
    }
});
	
$(document).foundation(); //Without Vue components, comment out above Vue call.
```

### Snackbar (Optional)
The Snackbar is a Vue component that is deployed as part of the Foundation package. During this install, you will be asked if you want to include it. The assets will be published with previous step, selecting yes will add the javascript and sass file to your project documents.

**resources\js\app.js**
```javascript
//add somewhere after the require('bootstrap')
require('./snackbar-loader');
```

**resources\sass\app.scss**
```php
@import "snackbar";  //end of file
```

Recompile your assets
```php
npm install && npm run dev
```

Snackbar will register a Vue event component and will listen for a message event which it while cycle through for the user. It will also listen in on your axios post for anything with an error or a json field named 'message' and automatically parse to the user.

```javascript
Event.$emit('message', "Message for user"); //single message
Event.$emit('messages', ['message 1', 'message 2']); //array of messages
```
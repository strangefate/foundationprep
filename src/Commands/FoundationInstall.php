<?php

namespace StrangeFate\FoundationPrep\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class FoundationInstall extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'foundation:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command installs the requirements for Zurb Foundation and Font Awesome';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $process = Process::fromShellCommandline('npm install --save-dev @fortawesome/fontawesome-free foundation-sites what-input motion-ui');

        $this->info("Adding Foundation to npm library.");

        $process->run(function ($type, $buffer) {
            if (Process::ERR === $type) {
                $this->error($buffer);
            } else {
                $this->line($buffer);
            }
        });

        $this->info("Coping foundation settings file");

        $file = file_get_contents( base_path('/node_modules/foundation-sites/scss/settings/_settings.scss') );
        $file = str_replace("@import 'util/util';", '@import "../../node_modules/foundation-sites/scss/util/util";', $file);
        file_put_contents(resource_path('sass/_settings.scss'), $file);

        $this->info("Calling command to publish views.");
        $this->call('vendor:publish', [
            '--provider' => 'StrangeFate\FoundationPrep\FoundationPrepServiceProvider',
            '--force' => true,
        ]);

        $this->info("Updating app.js file.");
        $this->fix_app_js_file();
        $this->info("Updating bootstrap.js file.");
        $this->fix_bootstrap_js_file();

        $this->info("Installation complete!");

        if ($this->confirm('Do you want to use Snackbar?')) {
            file_put_contents(
                resource_path('sass/app.scss'), 
                "\n@import \"snackbar\";",
                FILE_APPEND | LOCK_EX
            );

            $js = resource_path('/js/app.js');
            $lines = array();
            foreach(file($js) as $line) {
                // first switch these lines so you write the line and then add the new line after it
                array_push($lines, $line);

                // then test if the line contains variable
                if (strpos($line, "require('./bootstrap');") !== FALSE) {
                    array_push($lines, "require('./snackbar-loader');\n");
                }
            }
            file_put_contents($js, $lines);
        }

        $this->line("");
        //$this->error("Follup items");
    }

    private function fix_app_js_file() {
        $js = resource_path('/js/app.js');
        $lines = array();
        foreach(file($js) as $line) {
            // write current line to array
            array_push($lines, $line);

            if (strpos($line, "el: '#app',") !== FALSE) {
                array_push($lines, "     mounted() { \n" );
                array_push($lines, "        $(document).foundation(); // With Vue components \n");
                array_push($lines, "     }, \n");
            }
        }

        array_push($lines, "\n\n// $(document).foundation(); //Without Vue components. \n" );

        file_put_contents($js, $lines);
    }

    private function fix_bootstrap_js_file() {
        $js = resource_path('/js/bootstrap.js');
        $lines = array();
        foreach(file($js) as $line) {

            // write current line to array
            if (strpos($line, "window.Popper") === FALSE && strpos($line, "require('bootstrap');") === FALSE)
                array_push($lines, $line);

            if (strpos($line, "window.$ = window.jQuery") !== FALSE) {
                array_push($lines, "    require('foundation-sites'); \n" );
                array_push($lines, "    require('what-input'); \n");
            }
        }

        file_put_contents($js, $lines);
    }
}
<?php 
namespace StrangeFate\FoundationPrep;

use Illuminate\Support\ServiceProvider;
use StrangeFate\FoundationPrep\Commands\FoundationInstall;


class FoundationPrepServiceProvider extends ServiceProvider {
	public function boot() {
		//commands
		if( $this->app->runningInConsole() ) {
			$this->commands([
				FoundationInstall::class
			]);
		}

		$this->publishes([
			__DIR__.'/resources' => resource_path()
		], 'foundation-views');
	}

	public function register() {

	}
}
window.Event = new Vue();

Vue.component('snackbar', require('./components/Snackbar').default);

/**
* Extend axios to grab any messages or errors and forward them to snackbar.
*/
axios.interceptors.response.use( (r) => {
	if(r.data.hasOwnProperty('message'))
		Event.$emit('message', r.data.message);
	if(r.data.hasOwnProperty('messages'))
		Event.$emit('messages', r.data.messages);
	return r;
}, e => {
	console.log(e.response);  //for debug
	if(e.response.data.hasOwnProperty('errors')){
		var errors = [];
		var response = e.response.data.errors;
		for(var k in response)
			if(response[k] != 'message')
				errors.push(...response[k]);

		Event.$emit('messages', errors);
	}
	return Promise.reject(e);
});
@extends('layouts.master')

@section('content')
<div class="grid-x grid-padding-x align-center">
	<div class="callout cell medium-4 simple-layout">
		<h4>Login</h4>

		<form method="POST" action="{{route('login')}}">
			@csrf

			@if($errors->any())
			<div class="callout alert">
				<i class="fas fa-exclamation-triangle"></i> There are some errors in your request.
			</div>
			@endif

			<input type="email" required autofocus
				id="email" name="email" 
				value="{{ old('email') }}"
				placeholder="Enter your e-mail address here">

			@if($errors->has('email'))
			<span class="form-error is-visible">
				@foreach($errors->get('email') as $error)
					<strong>{{$error}}</strong><br />
				@endforeach
			</span>
			@endif

			<input type="password" required
				id="password" name="password"
				placeholder="Your password goes here">

			@if($errors->has('password'))
			<span class="form-error is-visible">
				@foreach($errors->get('password') as $error)
					<strong>{{$error}}</strong><br />
				@endforeach
			</span>
			@endif
			
			<div class="cell">
				<input class="button expanded small" type="submit" value="Submit">
			</div>

			<div class="grid-x">
				<fieldset class="cell small-6">
					<input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
					<label for="remember">Remember Me</label>
				</fieldset>

				<div class="cell small-6">
					<a href="{{ route('password.request') }}" 
						style="float: right; font-size: xx-small; font-style: italic;">
						Forgot Your Password?
					</a><br />
					<a href="{{ route('register') }}" style="float: right; font-size: xx-small; font-style: italic; position: relative; top: -10px;">
						Register
					</a>
				</div>
			</div>

		</form>
	</div>
</div>

@endsection
@extends('layouts.master')

@section('content')

@if(session('status'))
<div class="grid-x align-center">
    <div class="callout medium-4 success">
        {{ session('status') }}
    </div>
</div>
@endif

<div class="grid-x grid-padding-x align-center">
    <div class="callout cell medium-4 simple-layout">
        <h4>Reset Password</h4>
        <form method="POST" action="{{ route('password.email') }}">
            @csrf
        
            @if($errors->any())
            <div class="callout alert">
                <i class="fas fa-exclamation-triangle"></i> There are some errors in your request.
            </div>
            @endif

            <div class="cell">
                <label for="email">E-mail address
                    <input type="email" id="email" name="email"
                        required value="{{ old('email')}}">

                    @if($errors->has('email'))
                    <span class="form-error is-visible">
                        @foreach($errors->get('email') as $error)
                            <strong>{{$error}}</strong><br />
                        @endforeach
                    </span>
                    @endif
                </label>
            </div>

            <div class="cell">
                <input class="button expanded small" type="submit" value="Send Password Reset Link">
            </div>
        </form>
    </div>
</div>
@endsection

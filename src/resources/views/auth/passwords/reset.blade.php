@extends('layouts.master')

@section('content')

<div class="grid-x grid-padding-x align-center">
    <div class="callout cell medium-4 simple-layout">
        <h4>Reset Password</h4>

        <form method="POST" action="{{route('password.update')}}">
            @csrf

            @if($errors->any())
            <div class="callout alert">
                <i class="fas fa-exclamation-triangle"></i> There are some errors in your request.
            </div>
            @endif

            <input type="hidden" name="token" value="{{ $token }}">
            <div class="cell">
                <label for="email">E-mail address
                    <input type="email" id="email" name="email"
                        required value="{{ old('email')}}">

                    @if($errors->has('email'))
                    <span class="form-error is-visible">
                        @foreach($errors->get('email') as $error)
                            <strong>{{$error}}</strong><br />
                        @endforeach
                    </span>
                    @endif
                </label>
            </div>

            <div class="cell">
                <label for="password">Password
                    <input type="password" id="password" name="password" required>

                    @if($errors->has('password'))
                    <span class="form-error is-visible">
                        @foreach($errors->get('password') as $error)
                            <strong>{{$error}}</strong><br />
                        @endforeach
                    </span>
                    @endif
                </label>
            </div>

            <div class="cell">
                <label for="password_confirmation">Confirm Password
                    <input type="password" id="password_confirmation" name="password_confirmation" required>
                </label>
            </div>
        
            <div class="cell">
                <input class="button expanded small" type="submit" value="Reset Password">
            </div>
        </form>
    </div>
</div>
@endsection

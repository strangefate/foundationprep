@extends('layouts.master')

@section('content')
<div class="grid-x grid-padding-x align-center">
    <div class="callout cell medium-4 simple-layout">
        <h4>Register</h4>

        <form method="POST" action="{{ route('register') }}">
            @csrf
            
            @if($errors->any())
            <div class="callout alert">
                <i class="fas fa-exclamation-triangle"></i> There are some errors in your request.
            </div>
            @endif

            <div class="cell">
                <label for="name">Name
                    <input type="text" id="name" name="name" 
                        required autofocus value="{{old('name')}}">

                    @if($errors->has('name'))
                    <span class="form-error is-visible">
                        @foreach($errors->get('name') as $error)
                            <strong>{{$error}}</strong><br />
                        @endforeach
                    </span>
                    @endif
                </label>
            </div>
            
            <div class="cell">
                <label for="email">E-mail adress
                    <input type="text" id="email" name="email"
                        required value="{{ old('email')}}"
                        placeholder="This will be your login">

                    @if($errors->has('name'))
                    <span class="form-error is-visible">
                        @foreach($errors->get('email') as $error)
                            <strong>{{$error}}</strong><br />
                        @endforeach
                    </span>
                    @endif
                </label>
            </div>

            <div class="cell">
                <label for="password">Password
                    <input type="password" id="password" name="password" required>

                    @if($errors->has('password'))
                    <span class="form-error is-visible">
                        @foreach($errors->get('password') as $error)
                            <strong>{{$error}}</strong><br />
                        @endforeach
                    </span>
                    @endif
                </label>
            </div>

            <div class="cell">
                <label for="password_confirmation">Confirm Password
                    <input type="password" id="password_confirmation" name="password_confirmation" required>
                </label>
            </div>            
        
            <div class="cell">
                <input class="button expanded small" type="submit" value="Register">
            </div>
        </form>
    </div>
</div>
@endsection
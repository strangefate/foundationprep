
<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        @yield('title')       
        <title>{{env('APP_NAME')}}</title>

        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        {{-- favicon placeholder --}}
    </head>

    <body>
        
        @include('layouts.header')

        <div id="app" style="min-height: 50vh;">
            @yield('content')

            <snackbar></snackbar>
        </div>

        @include('layouts.footer')

        <script src="{{ asset('js/app.js') }}"></script>
        @stack('scripts')

    </body>
</html>
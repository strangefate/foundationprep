<div class="remove-menu-fouc">
	<div class="title-bar" data-responsive-toggle="settings-dashboard-menu" data-hide-for="large">
		<button class="menu-icon" type="button" data-toggle="settings-dashboard-menu"></button>
		<div class="title-bar-title"> Main Menu</div>
	</div>

	<div class="top-bar" id="settings-dashboard-menu">
		<div class="top-bar-left" >
			<ul class="menu vertical large-horizontal">
				<li>
					<a href="{{ url('/') }}">
						<i class="fas fa-home"></i> Home
					</a>
				</li> 

			</ul>
		</div>

		<div class="top-bar-right">
			<ul class="dropdown vertical large-horizontal menu" data-dropdown-menu>
				@guest
				<li>
					<a href="{{ route('login') }}">
						<i class="fas fa-sign-in-alt"></i> Login
					</a>
				</li>
				<li>
					<a href="{{ route('register') }}">
						<i class="fas fa-stream"></i> Register
					</a>
				</li>
				@else
				<li>
					<a onclick="$('#logout-form').submit()" class="hidden-button">
						<i class="fas fa-sign-out-alt"></i> Logout
					</a>
					<form id="logout-form" method="POST" action="{{ route('logout') }}">
						@csrf
					</form>
				</li>
				@endguest

				@can('administrate')
				<li>
					<a href="#">
						<i class="fas fa-user-shield"></i>
						Admin
					</a>
					<ul class="menu vertical">
						@can('viewTelescope')
						<li>
							<a href="/telescope" target="_blank" class="hidden-button">
								<i class="fas fa-chart-bar"></i> Logs
							</a>
						</li>
						@endcan
					</ul>
				</li>
				@endcan
			</ul>
		</div>
	</div>
</div>